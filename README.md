# ICFP 2007 Contest

I didn't compete in the contest, but I stumbled upon it one day and have found it to be a really fun project. Because of the performance issues of executing the DNA, it's been a great project to try out different ideas on.

Recently, I decided I should actually finish the DNA processor instead of just using it to mess around. I'm most comfortable with Java, so that's the language I used. On my laptop, the current implementation executes the base Endo DNA in about 8 seconds.

## DNA Processing

The basic approach I always take with projects is to get a working, naive version first and then use that to help debug the better version later. This problem deals with a lot of text operations (append, prepend, substring, etc) so Strings were the obvious choice for the simple approach.

I knew I would need to replace that implementation, so I hid everything behind interfaces. The DNA operations are mostly done through a custom iterator interface, which would allow me to easily switch implementations for debugging. It also helped make sure that I was doing things in close to O(n) time because the iterators only move forward.

Once the String version made it through 100,000 iterations, I figured it was good. Next was time to work on speeding it up.

The obvious speed up would be from appending and prepending the DNA. The simplest data structure that allows that in O(1) time is a linked list so that's what I went with. The DNA structure has head and tail members that point to Node objects. The Node objects have a next pointer and the data chunk, which I initially kept as Strings. That alone gave a decent speed boost but substring operations became a bottleneck.

To speed up substrings, I needed a class that would share an underlying buffer and only keep track of indices into that buffer. I couldn't find one in Java, so I rolled my own and called it SharedBuffer. Instead of copying the raw data, I would only need to copy the Node objects and update the SharedBuffer indices when necessary, namely at the start and end of a substring.

Here's what taking a substring basically looks like. Given the string "That is string", which is comprised of pieces of the strings "This is a string" and "That is not",

```
head          tail
  |            |
  V            V
 --     --     --
|  |-->|  |-->|  |-->
 --     --     --
 |      |      |
 V      V      V
 ---   ---   -----
|0|2| |2|8| |10|16|
 ---   ---   -----
 |      |     | 
 |      +-----|-------+
 +-+       +--+       |
   |       |          |
   V       V          V
 "This is a string" "That is not"
```
To get the substring "hat is s" requires copying the Nodes and updating the first and last SharedBuffers. The Nodes need to be copied so that they're not shared between different substrings. Had a nasty bug because of that one. Anyway, the substring ends up being,
```
head          tail
  |            |
  V            V
 --     --     --
|  |-->|  |-->|  |-->
 --     --     --
 |      |      |
 V      V      V
 ---   ---   -----
|1|2| |2|8| |10|11|
 ---   ---   -----
 |      |     | 
 |      +-----|-------+
 +-+       +--+       |
   |       |          |
   V       V          V
 "This is a string" "That is not"
```

No actual data gets copied. There were some fun bugs around getting the tail pointer updated correctly, which took a little time to hammer out, but it was still slower than I wanted. It ran in 43s, which is probably good enough but I thought it should be faster than that.

The main culprit turned out to be the fact that a lot of single character entries get added and copied, which meant my linked list was getting too long and was full of nodes pointing to single characters. Completely defeats the purpose since it made things O(n) in the size of the total string instead of O(n) in the number of nodes.

The solution to that was to combine nodes. I tried a couple different ways of doing it and ended up with a hack. Calculating the length of the DNA requires the entire linked list to be walked. Since we're walking it anyway, I had it also count the number of nodes. If that number got above 200, combine them all into one node.

That finally did it. It runs in under 10 seconds, which I'm more than happy with.

## RNA Processing

Processing the RNA was much simpler. A straightforward implementation worked pretty well. The recursive flood fill algorithm overflowed the stack, but a simple iterative implementation got around that and wasn't a bad bottle neck. I didn't want to go the suggested route of writing the RNA to disk and processing it later so it gets processed at the end of each loop.

I had some issues because I implemented it too fast and made some mistakes, but it really wasn't bad. On the list of things to do is implement a better flood fill, but it's not high on the list because it's not a pain point yet.

## Display

I wanted to watch the image being drawn so wanted a UI that I could draw the image to as it was processing. Desktop apps are going the way of the dinosaur, so I wanted to have a web based solution. Writing a server in java is beyond annoying, so I went with node. It calls the java program when the user clicks start and the java program writes json to standard out. That json consists of the stats, loop number, cost, rna count, and the image file to display. The RNA processor writes a PNG image every time it does any composition and that image gets displayed.

Even with doing that and being able to watch the image get drawn, the endo image was still around 15 seconds to process.

That got me to where I can start the reverse endineering part of the problem and try to figure out how this all works and fits together.
