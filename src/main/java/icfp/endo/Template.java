package icfp.endo;

import java.util.ArrayList;
import java.util.List;
import icfp.endo.Tokenizer.Token;

/**
 * Template
 */
public class Template {
    public static class Item {
        private char base;
        private int level;
        private int num;
        private int lenIndex;

        public Item(char ch) {
            this(ch, -1, -1, -1);
        }

        public Item(int index) {
            this('\0', index, -1, -1);
        }

        public Item(int level, int num) {
            this('\0', -1, level, num);
        }

        private Item(char ch, int lenIndex, int level, int num) {
            this.base = ch;
            this.lenIndex = lenIndex;
            this.level = level;
            this.num = num;
        }

        public String toString() {
            if (hasBase()) {
                return "" + base;
            } else if (hasLengthIndex()) {
                return "|" + lenIndex + "|";
            } else if (hasReference()) {
                return "[" + num + "_" + level + "]";
            } else {
                return "";
            }
        }

        public boolean hasBase() {
            return base != '\0';
        }

        public boolean hasLengthIndex() {
            return lenIndex >= 0;
        }

        public boolean hasReference() {
            return (level >= 0 && num >= 0);
        }

        public char getBase() {
            return base;
        }

        public int getLenIndex() {
            return lenIndex;
        }

        public int getLevel() {
            return level;
        }

        public int getNumber() {
            return num;
        }
    }

    public Template(DnaObserver observer) {
        items = new ArrayList<>();
        shouldReturn = false;
        this.observer = observer;
    }

    public void parse(DNAIterator iter) {
        var tokenizer = new Tokenizer();

        while (true) {
            var token = tokenizer.nextToken(iter);
            if (token == Token.NONE) {
                observer.finish();
                throw new RuntimeException("TEMPLATE NO TOKEN");
            }

            var item = getItem(token, iter);

            if (shouldReturn) {
                return;
            }

            if (item != null) {
                items.add(item);
            }
        }
    }

    public List<Item> getItems() {
        return items;
    }

    private Item getItem(Token token, DNAIterator iter) {
        switch (token) {
        case C:
            observer.addCost(1);
            return new Item('I');
        case F:
            observer.addCost(1);
            return new Item('C');
        case P:
            observer.addCost(1);
            return new Item('F');
        case IC:
            observer.addCost(2);
            return new Item('P');
        case IF:
        case IP:
            observer.addCost(2);
            return new Item(Helpers.nat(iter, observer), Helpers.nat(iter, observer));
        case IIC:
        case IIF:
            observer.addCost(3);
            shouldReturn = true;
            return null;
        case IIP:
            observer.addCost(3);
            return new Item(Helpers.nat(iter, observer));
        case III:
            observer.rna(Helpers.rnaToBase4(iter));
            observer.addCost(10);
            return null;
        default:
            observer.finish();
            throw new RuntimeException("Template FINISH");
        }
    }

    @Override
    public String toString() {
        var str = new StringBuilder();

        for (var item : items) {
            str.append(item.toString());
        }

        return str.toString();
    }

    private List<Item> items;
    private boolean shouldReturn;
    private DnaObserver observer;
}
