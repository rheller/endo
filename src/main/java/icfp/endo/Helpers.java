package icfp.endo;

import java.util.HashMap;
import java.util.Map;
import icfp.endo.fastdna.FastDNA;

/**
 * Helpers
 */
public class Helpers {
    public static boolean DEBUG = false;

    public static DNA createDNA() {
        return new FastDNA();
        // return new StringDNA();
    }

    public static int rnaToBase4(DNAIterator iter) {
        var value = 0;

        for (var count = 0; count < 7; count += 1) {
            if (!iter.hasNext()) {
                break;
            }

            var ch = iter.next();
            value = (value * 4) + toBase4(ch);
        }

        return value;
    }

    public static int stringToBase4(String str) {
        var value = 0;

        for (var ndx = 0; ndx < str.length(); ndx += 1) {
            value = (value * 4) + toBase4(str.charAt(ndx));
        }

        return value;
    }

    public static String base4ToString(int n, int padTo) {
        var str = new StringBuilder();

        while (n > 0) {
            var digit = n & 0x3;
            var ch = 'I';

            if (digit == 1) {
                ch = 'C';
            } else if (digit == 2) {
                ch = 'F';
            } else if (digit == 3) {
                ch = 'P';
            }

            str.append(ch);
            n >>= 2;
        }

        str = str.reverse();
        for (var loop = str.length(); loop < padTo; loop += 1) {
            var tmp = new StringBuilder();
            tmp.append('I').append(str);
            str = tmp;
        }

        return str.toString();
    }

    public static int toBase4(char ch) {
        if (ch == 'I') {
            return 0;
        } else if (ch == 'C') {
            return 1;
        } else if (ch == 'F') {
            return 2;
        } else if (ch == 'P') {
            return 3;
        }

        return 0;
    }

    public static int nat(DNAIterator iter, DnaObserver observer) {
        var base = 1;
        var n = 0;
        var count = 0;

        while (true) {
            if (!iter.hasNext()) {
                observer.finish();
                throw new RuntimeException("Helpers.nat FINISH");
            }

            var ch = iter.next();
            count += 1;

            if (ch == 'P') {
                break;
            } else if (ch == 'C') {
                n += base;
            }

            base <<= 1;
        }

        observer.addCost(count);
        return n;
    }

    public static String consts(DNAIterator iter, DnaObserver observer) {
        var str = new StringBuilder();
        var nextIter = iter.copy();
        var count = 0;

        nextIter.next();
        count += 1;
        while (true) {
            char ch = iter.peek();

            if (ch == 'I') {
                if (nextIter.peek() != 'C') {
                    break;
                }

                count += 1;
                iter.next();
                nextIter.next();
                str.append('P');
            } else if (ch == 'C') {
                str.append('I');
            } else if (ch == 'F') {
                str.append('C');
            } else if (ch == 'P') {
                str.append('F');
            } else {
                break;
            }

            count += 1;
            iter.next();
            nextIter.next();
        }

        observer.addCost(count);
        return str.toString();
    }

    public static DNA protect(int level, DNA e, DnaObserver observer) {
        if (level == 0) {
            return e;
        }

        for (var loop = 0; loop < level; loop += 1) {
            var iter = e.getIterator();
            var str = new StringBuilder();

            while (iter.hasNext()) {
                var ch = iter.next();
                switch (ch) {
                case 'I':
                    str.append('C');
                    break;
                case 'C':
                    str.append('F');
                    break;
                case 'F':
                    str.append('P');
                    break;
                case 'P':
                    str.append("IC");
                    break;
                }
            }

            e = Helpers.createDNA();
            e.prepend(str.toString().getBytes());
        }

        observer.addCost(e.length());
        return e;
    }

    public static String asnat(int length) {
        var str = new StringBuilder();

        while (length > 0) {
            if ((length & 1) == 1) {
                str.append('C');
            } else {
                str.append('I');
            }

            length >>= 1;
        }

        str.append('P');

        return str.toString();
    }

    public static DNAIterator search(DNAIterator iter, String srch) {
        var shift = buildShiftTable(srch);
        var j = 0;
        var iIter = iter.copy();
        var jIter = iter.copy();

        while (iIter.hasNext()) {
            while (jIter.peek() == srch.charAt(j)) {
                j += 1;
                jIter.next();

                if (j >= srch.length()) {
                    return iIter;
                }
            }

            if (j > 0) {
                iIter.seek(shift[j - 1]);

                j = Math.max(0, j - shift[j - 1]);
                jIter = iIter.copy();
                jIter.seek(j);
            } else {
                iIter.next();
                jIter = iIter.copy();
            }
        }

        return null;
    }

    private static int[] buildShiftTable(String pattern) {
        var shift = new int[pattern.length()];
        var i = 1;
        var j = 0;

        shift[0] = 1;

        while ((i + j) < pattern.length()) {
            if (pattern.charAt(i + j) == pattern.charAt(j)) {
                shift[i + j] = i;
                j += 1;
            } else {
                if (j == 0) {
                    shift[i] = i + 1;
                }

                if (j > 0) {
                    i += shift[j - 1];
                    j = Math.max(0, j - shift[j - 1]);
                } else {
                    i += 1;
                    j = 0;
                }
            }
        }

        return shift;
    }

    private static Map<String, Long> profData = new HashMap<>();

    public static void profile(String label, Runnable code) {
        var start = System.currentTimeMillis();

        code.run();

        var diff = System.currentTimeMillis() - start;
        var v = (long) 0;

        if (profData.containsKey(label)) {
            v = profData.get(label);
        }

        profData.put(label, v + diff);
    }

    public static void printProfileData() {
        System.out.println("PROFILE");

        for (var label : profData.keySet()) {
            System.out.println("-- " + label + ": " + profData.get(label) + " ms");
        }
    }
}
