package icfp.endo;

/**
 * Tokenizer
 */
public class Tokenizer {
    public enum Token {
        C, F, P, IC, IF, IP, IIC, IIF, IIP, III, NONE
    }

    public Tokenizer() {
    }

    public Token nextToken(DNAIterator iter) {
        if (!iter.hasNext()) {
            return Token.NONE;
        }

        var ch = iter.next();

        switch (ch) {
        case 'C':
            return Token.C;
        case 'F':
            return Token.F;
        case 'P':
            return Token.P;
        case 'I':
            return nextTokenI(iter);
        default:
            throw new RuntimeException("Tokenizer.nextToken FINISH");
        }
    }

    private Token nextTokenI(DNAIterator iter) {
        var ch = iter.next();

        switch (ch) {
        case 'C':
            return Token.IC;
        case 'F':
            return Token.IF;
        case 'P':
            return Token.IP;
        case 'I':
            return nextTokenII(iter);
        default:
            throw new RuntimeException("Tokenizer.nextTokenI FINISH");
        }
    }

    private Token nextTokenII(DNAIterator iter) {
        var ch = iter.next();

        switch (ch) {
        case 'C':
            return Token.IIC;
        case 'F':
            return Token.IIF;
        case 'P':
            return Token.IIP;
        case 'I':
            return Token.III;
        default:
            throw new RuntimeException("Tokenizer.nextTokenII FINISH");
        }
    }
}
