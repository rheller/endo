package icfp.endo;

/**
 * StringDNA
 */
public class StringDNA implements DNA {
    private String data;

    /**
     * Iter
     */
    public class Iter implements DNAIterator {
        private int offset;

        public Iter() {
            offset = 0;
        }

        public Iter(DNAIterator copy) {
            var iter = (Iter) copy;

            offset = iter.offset;
        }

        @Override
        public int getPosition() {
            return offset;
        }

        @Override
        public DNA getDNA() {
            return StringDNA.this;
        }

        @Override
        public void truncate() {
            data = data.substring(offset);
            offset = 0;
        }

        @Override
        public DNA substring(DNAIterator iter) {
            var end = (Iter) iter;
            var dna = Helpers.createDNA();

            dna.prepend(data.substring(offset, end.getPosition()));

            return dna;
        }

        @Override
        public boolean isValid() {
            return offset >= 0 && offset <= data.length();
        }

        @Override
        public char peek() {
            if (!hasNext()) {
                return '\0';
            }

            return data.charAt((int) offset);
        }

        @Override
        public char peek(int offset) {
            return data.charAt(this.offset + offset);
        }

        @Override
        public void seek(int jump) {
            offset += jump;

            if (offset > data.length()) {
                offset = data.length();
            }
        }

        @Override
        public DNAIterator copy() {
            var iter = new Iter();
            iter.offset = this.offset;
            return iter;
        }

        @Override
        public boolean hasNext() {
            return (offset >= 0 && offset < data.length());
        }

        @Override
        public Character next() {
            var ch = data.charAt((int) offset);

            offset += 1;

            return ch;
        }
    }

    public StringDNA() {
        data = "";
    }

    @Override
    public void append(String toAppend) {
        data = data + toAppend;
    }

    @Override
    public void append(byte[] toAppend) {
        data = data + new String(toAppend);
    }

    @Override
    public void append(DNA toPrepend) {
        var other = (StringDNA) toPrepend;
        data = data + other.data;
    }

    @Override
    public void prepend(String toPrepend) {
        data = toPrepend + data;
    }

    @Override
    public void prepend(byte[] toPrepend) {
        data = new String(toPrepend) + data;
    }

    @Override
    public void prepend(DNA toPrepend) {
        var other = (StringDNA) toPrepend;
        data = other.data + data;
    }

    @Override
    public DNAIterator getIterator() {
        return new Iter();
    }

    @Override
    public DNAIterator copyIterator(DNAIterator iter) {
        return new Iter(iter);
    }

    @Override
    public int length() {
        return data.length();
    }

    @Override
    public String toString() {
        return data;
    }

    @Override
    public String debugString() {
        var str = new StringBuilder();

        if (data.length() >= 10) {
            str.append(data.substring(0, 10));
        } else {
            str.append(data);
        }

        if (data.length() > 10) {
            str.append("...");
        }

        str.append("(").append(data.length()).append(")");

        return str.toString();
    }
}
