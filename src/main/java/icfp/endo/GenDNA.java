package icfp.endo;

import java.io.BufferedWriter;
import java.io.FileWriter;
import java.nio.file.Paths;
import java.util.ArrayList;
import java.util.List;

/**
 * GenDNA
 */
public class GenDNA {
    public static void writeDNA(BufferedWriter writer, List<String> dna) throws Exception {
        for (var item : dna) {
            writer.write(item);
        }
    }

    public static List<Integer> getRnaCommands() {
        var list = new ArrayList<Integer>();

        list.add(RNAProcessor.CMD_ADD_RED);
        list.add(RNAProcessor.CMD_MARK);
        for (var c = 0; c < 40; c += 1) {
            list.add(RNAProcessor.CMD_MOVE);
        }

        list.add(RNAProcessor.CMD_TURN_CCW);
        for (var c = 0; c < 40; c += 1) {
            list.add(RNAProcessor.CMD_MOVE);
        }

        list.add(RNAProcessor.CMD_LINE);

        return list;
    }

    public static List<String> toDnaList(List<Integer> rna) {
        var list = new ArrayList<String>();

        for (var item : rna) {
            list.add(Helpers.base4ToString(item, 10));
        }

        return list;
    }

    public static void main(String[] args) {
        if (args.length == 0) {
            System.out.println("Please give a file name");
            return;
        }

        var path = Paths.get("dna", args[0]);

        try (var writer = new BufferedWriter(new FileWriter(path.toString()))) {
            var rna = GenDNA.getRnaCommands();
            var dna = GenDNA.toDnaList(rna);

            for (var item : dna) {
                writer.write(item);
            }
        } catch (Exception ex) {
            System.out.println("ERROR: " + ex);
        }
    }
}
