package icfp.endo;

import java.util.Iterator;

/**
 * DNAIterator
 */
public interface DNAIterator extends Iterator<Character> {
    DNA getDNA();

    DNAIterator copy();

    char peek();

    char peek(int offset);

    void seek(int jump);

    boolean isValid();

    DNA substring(DNAIterator end);

    void truncate();

    int getPosition();
}
