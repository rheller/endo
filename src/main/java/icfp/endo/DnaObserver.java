package icfp.endo;

/**
 * Observer
 */
public interface DnaObserver {
    void addCost(int value);

    void rna(int rna);

    void finish();

    void incLoop();

    int getRnaCount();

    int getCost();

    int getLoop();
}
