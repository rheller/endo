package icfp.endo;

import java.awt.image.BufferedImage;
import java.io.File;
import java.io.IOException;
import java.nio.file.Files;
import java.nio.file.Paths;
import java.util.ArrayList;
import java.util.List;
import javax.imageio.ImageIO;
import com.google.gson.Gson;
import com.google.gson.GsonBuilder;
import com.google.gson.annotations.SerializedName;

/**
 * Hello world!
 *
 */
public class App implements DnaObserver, RnaObserver {
    public App() {
        loop = 0;
        cost = 0;
        statsFrequency = -1;
        rnaList = new ArrayList<>();
        rnaProcessor = new RNAProcessor(this);
        gson = new GsonBuilder().create();
        imageNumber = 0;
    }

    public static byte[] getFileBytes(String fileName) throws IOException {
        var path = Paths.get(fileName);
        return Files.readAllBytes(path);
    }

    private class StatsUpdate {
        StatsUpdate(int loop, int rna, int cost, String image) {
            this.loop = loop;
            this.rna = rna;
            this.cost = cost;
            this.image = image;
        }

        @SerializedName("loops")
        int loop;
        @SerializedName("rna")
        int rna;
        @SerializedName("cost")
        int cost;
        @SerializedName("image")
        String image;
    }

    private void jsonStats(String image) {
        var stats = new StatsUpdate(loop, rnaCount, cost, image);
        var json = gson.toJson(stats);

        System.out.println(json);
    }

    @Override
    public void image(int[][] img) {
        var filePath = Paths.get("images", getImgFileName());
        var path = Paths.get("static", filePath.toString());

        try {
            var buf = new BufferedImage(img.length, img.length, BufferedImage.TYPE_INT_ARGB);

            for (var x = 0; x < img.length; x += 1) {
                for (var y = 0; y < img.length; y += 1) {
                    buf.setRGB(x, y, img[x][y]);
                }
            }

            ImageIO.write(buf, "png", new File(path.toString()));

            jsonStats(filePath.toString());
        } catch (Exception ex) {
            throw new RuntimeException("App.image FAILED TO WRITE FILE " + ex);
        }
    }

    private String getImgFileName() {
        imageNumber += 1;
        return "" + imageNumber + ".png";
    }

    @Override
    public void finish() {
        if (rnaList.size() > 0) {
            rnaProcessor.process(rnaList);
        }

        image(rnaProcessor.getFinalImage());

        System.exit(0);
    }

    @Override
    public void incLoop() {
        loop += 1;

        if (rnaList.size() > 0) {
            rnaProcessor.process(rnaList);
            rnaList.clear();
        }

        if (statsFrequency >= 0 && loop % statsFrequency == 0) {
            image(rnaProcessor.getImage());
        }
    }

    @Override
    public int getLoop() {
        return loop;
    }

    @Override
    public void addCost(int value) {
        cost += value;
    }

    @Override
    public int getCost() {
        return cost;
    }

    @Override
    public int getRnaCount() {
        return rnaList.size();
    }

    @Override
    public void rna(int rna) {
        rnaCount += 1;
        rnaList.add(rna);
    }

    public void parseArgs(String[] args) {
        var ndx = 0;

        while (ndx < args.length) {
            if ("-dna".equals(args[ndx])) {
                ndx += 1;
                dnaFile = args[ndx];
            } else if ("-prefix".equals(args[ndx])) {
                ndx += 1;
                prefixFile = args[ndx];
            } else if ("-stats".equals(args[ndx])) {
                ndx += 1;
                statsFrequency = Integer.parseInt(args[ndx]);
            }

            ndx += 1;
        }
    }

    public void run() throws IOException {
        var dna = Helpers.createDNA();

        if (dnaFile == null) {
            return;
        }

        if (prefixFile != null) {
            dna.append(getFileBytes(prefixFile));
        }

        dna.append(getFileBytes(dnaFile));

        var processor = new DNAProcessor(dna, this);
        processor.process();
    }

    private int loop;
    private int cost;
    private String dnaFile;
    private String prefixFile;
    private int statsFrequency;
    private int rnaCount;
    private List<Integer> rnaList;
    private RNAProcessor rnaProcessor;
    private Gson gson;
    private int imageNumber;

    public static void main(String[] args) throws Exception {
        var app = new App();

        app.parseArgs(args);

        app.run();
    }
}
