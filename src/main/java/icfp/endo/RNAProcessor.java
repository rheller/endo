package icfp.endo;

import java.util.ArrayList;
import java.util.List;
import java.util.Stack;

/**
 * RNAProcessor
 */
public class RNAProcessor {
    public RNAProcessor(RnaObserver observer) {
        this.observer = observer;

        bucket = new ArrayList<>();
        alphas = new ArrayList<>();
        bitmaps = new ArrayList<>();
        pos = new int[] {0, 0};
        mark = new int[] {0, 0};
        dir = Dir.EAST;

        bitmaps.add(new Bitmap());
    }

    public void process(List<Integer> rna) {
        for (var cmd : rna) {
            processCommand(cmd);
        }
    }

    public int[][] getImage() {
        return firstBitmap();
    }

    public int[][] getFinalImage() {
        var bm = firstBitmap();

        for (var x = 0; x < 600; x += 1) {
            for (var y = 0; y < 600; y += 1) {
                bm[x][y] |= ALPHA_MASK;
            }
        }

        return bm;
    }

    private int[][] firstBitmap() {
        var bitmap = bitmaps.get(bitmaps.size() - 1);

        return bitmap.map;
    }

    private void processCommand(int cmd) {
        if (cmd == CMD_ADD_BLACK) {
            bucket.add(COLOR_BLACK);
        } else if (cmd == CMD_ADD_RED) {
            bucket.add(COLOR_RED);
        } else if (cmd == CMD_ADD_GREEN) {
            bucket.add(COLOR_GREEN);
        } else if (cmd == CMD_ADD_BLUE) {
            bucket.add(COLOR_BLUE);
        } else if (cmd == CMD_ADD_YELLOW) {
            bucket.add(COLOR_YELLOW);
        } else if (cmd == CMD_ADD_MAGENTA) {
            bucket.add(COLOR_MAGENTA);
        } else if (cmd == CMD_ADD_CYAN) {
            bucket.add(COLOR_CYAN);
        } else if (cmd == CMD_ADD_WHITE) {
            bucket.add(COLOR_WHITE);
        } else if (cmd == CMD_ADD_TRANSPARENT) {
            alphas.add(TRANSPARENT);
        } else if (cmd == CMD_ADD_OPAQUE) {
            alphas.add(OPAQUE);
        } else if (cmd == CMD_TURN_CW) {
            turnCW();
        } else if (cmd == CMD_TURN_CCW) {
            turnCCW();
        } else if (cmd == CMD_MOVE) {
            move();
        } else if (cmd == CMD_MARK) {
            mark();
        } else if (cmd == CMD_LINE) {
            line();
        } else if (cmd == CMD_ADD_BITMAP) {
            addBitmap(new Bitmap());
        } else if (cmd == CMD_CLEAR_BUCKET) {
            clearBucket();
        } else if (cmd == CMD_TRYFILL) {
            tryfill();
        } else if (cmd == CMD_COMPOSE) {
            compose();
        } else if (cmd == CMD_CLIP) {
            clip();
        } else {
            // throw new RuntimeException("UNHANDLED RNA COMMAND " + Helpers.base4ToString(cmd, 0));
        }
    }

    private void clip() {
        if (bitmaps.size() < 2) {
            return;
        }

        var bm0 = popBitmap();
        var bm1 = popBitmap();

        for (var x = 0; x < 600; x += 1) {
            for (var y = 0; y < 600; y += 1) {
                var alpha0 = alphaValue(bm0.map[x][y]);
                var alpha1 = alphaValue(bm1.map[x][y]);
                var red1 = redValue(bm1.map[x][y]);
                var green1 = greenValue(bm1.map[x][y]);
                var blue1 = blueValue(bm1.map[x][y]);

                var alpha = (int) Math.floor(alpha1 * alpha0 / 255);
                var red = (int) Math.floor(red1 * alpha0 / 255);
                var green = (int) Math.floor(green1 * alpha0 / 255);
                var blue = (int) Math.floor(blue1 * alpha0 / 255);

                bm1.map[x][y] = makeColor(alpha, red, green, blue);
            }
        }

        addBitmap(bm1);

        observer.image(firstBitmap());
    }

    private void compose() {
        if (bitmaps.size() < 2) {
            return;
        }

        var bm0 = popBitmap();
        var bm1 = popBitmap();

        for (var x = 0; x < 600; x += 1) {
            for (var y = 0; y < 600; y += 1) {
                var alpha0 = alphaValue(bm0.map[x][y]);
                var red0 = redValue(bm0.map[x][y]);
                var green0 = greenValue(bm0.map[x][y]);
                var blue0 = blueValue(bm0.map[x][y]);
                var alpha1 = alphaValue(bm1.map[x][y]);
                var red1 = redValue(bm1.map[x][y]);
                var green1 = greenValue(bm1.map[x][y]);
                var blue1 = blueValue(bm1.map[x][y]);

                var alpha = alpha0 + (int) Math.floor(alpha1 * (255 - alpha0) / 255);
                var red = red0 + (int) Math.floor(red1 * (255 - alpha0) / 255);
                var green = green0 + (int) Math.floor(green1 * (255 - alpha0) / 255);
                var blue = blue0 + (int) Math.floor(blue1 * (255 - alpha0) / 255);

                bm1.map[x][y] = makeColor(alpha, red, green, blue);
            }
        }

        addBitmap(bm1);

        observer.image(firstBitmap());
    }

    private int makeColor(int alpha, int red, int green, int blue) {
        return (alpha << ALPHA_SHIFT) + (red << RED_SHIFT) + (green << GREEN_SHIFT)
                + (blue << BLUE_SHIFT);
    }

    private int colorValue(int color, int mask, int shift) {
        return (color & mask) >>> shift;
    }

    private int alphaValue(int color) {
        return colorValue(color, ALPHA_MASK, ALPHA_SHIFT);
    }

    private int redValue(int color) {
        return colorValue(color, RED_MASK, RED_SHIFT);
    }

    private int greenValue(int color) {
        return colorValue(color, GREEN_MASK, GREEN_SHIFT);
    }

    private int blueValue(int color) {
        return colorValue(color, BLUE_MASK, BLUE_SHIFT);
    }

    private void tryfill() {
        var newPixel = currentPixel();
        var old = getPixel(pos);

        if (old != newPixel) {
            fill(pos, old);
        }
    }

    private void fill(int[] pos, int old) {
        if (getPixel(pos) != old) {
            return;
        }

        var stack = new Stack<int[]>();

        stack.push(pos);
        while (!stack.empty()) {
            var curPos = stack.pop();

            if (getPixel(curPos) == old) {
                setPixel(curPos[0], curPos[1]);
            }

            var newPos = new int[] {curPos[0] - 1, curPos[1]};
            if (curPos[0] > 0 && getPixel(newPos) == old) {
                stack.push(newPos);
            }

            newPos = new int[] {curPos[0] + 1, curPos[1]};
            if (curPos[0] < 599 && getPixel(newPos) == old) {
                stack.push(newPos);
            }
            newPos = new int[] {curPos[0], curPos[1] - 1};
            if (curPos[1] > 0 && getPixel(newPos) == old) {
                stack.push(newPos);
            }
            newPos = new int[] {curPos[0], curPos[1] + 1};
            if (curPos[1] < 599 && getPixel(newPos) == old) {
                stack.push(newPos);
            }
        }
    }

    private int getPixel(int[] pos) {
        var x = pos[0];
        var y = pos[1];
        var bitmap = firstBitmap();

        return bitmap[x][y];
    }

    private void clearBucket() {
        bucket = new ArrayList<>();
        alphas = new ArrayList<>();
    }

    private void addBitmap(Bitmap bitmap) {
        if (bitmaps.size() < 10) {
            bitmaps.add(bitmap);
        }
    }

    private Bitmap popBitmap() {
        var bm = bitmaps.get(bitmaps.size() - 1);

        bitmaps.remove(bitmaps.size() - 1);

        return bm;
    }

    private void line() {
        var x0 = pos[0];
        var y0 = pos[1];
        var x1 = mark[0];
        var y1 = mark[1];
        var deltax = x1 - x0;
        var deltay = y1 - y0;
        var d = Math.max(Math.abs(deltax), Math.abs(deltay));
        var c = deltax * deltay <= 0 ? 1 : 0;
        var x = x0 * d + Math.floor((d - c) / 2);
        var y = y0 * d + Math.floor((d - c) / 2);

        for (var count = 0; count < d; count += 1) {
            setPixel((int) Math.floor(x / d), (int) Math.floor(y / d));
            x += deltax;
            y += deltay;
        }

        setPixel(x1, y1);
    }

    private void setPixel(int x, int y) {
        var bitmap = firstBitmap();

        bitmap[x][y] = currentPixel();
    }

    protected int currentPixel() {
        var rgb = averageRGB(bucket);
        var alpha = averageAlpha(alphas);

        var red = redValue(rgb);
        var green = greenValue(rgb);
        var blue = blueValue(rgb);

        red = (int) Math.floor(red * alpha / 255);
        green = (int) Math.floor(green * alpha / 255);
        blue = (int) Math.floor(blue * alpha / 255);

        return makeColor(alpha, red, green, blue);
    }

    private int averageAlpha(List<Integer> list) {
        if (list.size() == 0) {
            return 255;
        }

        var alpha = 0;
        for (var item : list) {
            alpha += item;
        }

        alpha = (int) Math.floor(alpha / list.size());

        if (alpha > 255) {
            throw new RuntimeException("INVALID ALPHA " + alpha);
        }

        return alpha;
    }

    private int averageRGB(List<Integer> list) {
        if (list.size() == 0) {
            return 0;
        }

        var red = 0;
        var green = 0;
        var blue = 0;
        for (var item : list) {
            red += redValue(item);
            green += greenValue(item);
            blue += blueValue(item);
        }

        red = (int) Math.floor(red / list.size());
        green = (int) Math.floor(green / list.size());
        blue = (int) Math.floor(blue / list.size());

        if (red > 255 || green > 255 || blue > 255) {
            throw new RuntimeException("INVALID COLOR " + red + " " + green + " " + blue);
        }

        return (red << RED_SHIFT) + (green << GREEN_SHIFT) + (blue << BLUE_SHIFT);
    }

    private void mark() {
        mark[0] = pos[0];
        mark[1] = pos[1];
    }

    private void move() {
        if (dir == Dir.NORTH) {
            pos[1] = wrap(pos[1] - 1);
        } else if (dir == Dir.EAST) {
            pos[0] = wrap(pos[0] + 1);
        } else if (dir == Dir.SOUTH) {
            pos[1] = wrap(pos[1] + 1);
        } else if (dir == Dir.WEST) {
            pos[0] = wrap(pos[0] - 1);
        }
    }

    private int wrap(int pos) {
        if (pos < 0) {
            pos = 599;
        }

        if (pos >= 600) {
            pos = 0;
        }

        return pos;
    }

    private void turnCW() {
        switch (dir) {
        case NORTH:
            dir = Dir.EAST;
            break;
        case SOUTH:
            dir = Dir.WEST;
            break;
        case EAST:
            dir = Dir.SOUTH;
            break;
        case WEST:
            dir = Dir.NORTH;
            break;
        }
    }

    private void turnCCW() {
        switch (dir) {
        case NORTH:
            dir = Dir.WEST;
            break;
        case SOUTH:
            dir = Dir.EAST;
            break;
        case EAST:
            dir = Dir.NORTH;
            break;
        case WEST:
            dir = Dir.SOUTH;
            break;
        }
    }

    private String cmdToString(int cmd) {
        if (cmd == CMD_ADD_BLACK) {
            return "CMD_ADD_BLACK";
        } else if (cmd == CMD_ADD_RED) {
            return "CMD_ADD_RED";
        } else if (cmd == CMD_ADD_GREEN) {
            return "CMD_ADD_GREEN";
        } else if (cmd == CMD_ADD_YELLOW) {
            return "CMD_ADD_YELLO";
        } else if (cmd == CMD_ADD_BLUE) {
            return "CMD_ADD_BLUE";
        } else if (cmd == CMD_ADD_MAGENTA) {
            return "CMD_ADD_MAGENTA";
        } else if (cmd == CMD_ADD_CYAN) {
            return "CMD_ADD_VYAN";
        } else if (cmd == CMD_ADD_WHITE) {
            return "CMD_ADD_WHITE";
        } else if (cmd == CMD_ADD_TRANSPARENT) {
            return "CMD_ADD_TRANSPARENT";
        } else if (cmd == CMD_ADD_OPAQUE) {
            return "CMD_ADD_OPAQUE";
        } else if (cmd == CMD_CLEAR_BUCKET) {
            return "CMD_CLEAR_BUCKET";
        } else if (cmd == CMD_MOVE) {
            return "CMD_MOVE";
        } else if (cmd == CMD_TURN_CCW) {
            return "CMD_TURN_CCW";
        } else if (cmd == CMD_TURN_CW) {
            return "CMD_TURN_CW";
        } else if (cmd == CMD_MARK) {
            return "CMD_MARK";
        } else if (cmd == CMD_LINE) {
            return "CMD_LINE";
        } else if (cmd == CMD_TRYFILL) {
            return "CMD_TRYFILL";
        } else if (cmd == CMD_ADD_BITMAP) {
            return "CMD_ADD_BITMAP";
        } else if (cmd == CMD_COMPOSE) {
            return "CMD_COMPOSE";
        } else if (cmd == CMD_CLIP) {
            return "CMD_CLIP";
        } else {
            return "UNKNOWN COMMAND";
        }
    }

    private enum Dir {
        NORTH, SOUTH, EAST, WEST
    }

    private class Bitmap {
        Bitmap() {
            map = new int[600][600];
            for (var x = 0; x < 600; x += 1) {
                for (var y = 0; y < 600; y += 1) {
                    map[x][y] = 0;
                }
            }
        }

        int[][] map;
    }

    public final static int CMD_ADD_BLACK = Helpers.stringToBase4("PIPIIIC");
    public final static int CMD_ADD_RED = Helpers.stringToBase4("PIPIIIP");
    public final static int CMD_ADD_GREEN = Helpers.stringToBase4("PIPIICC");
    public final static int CMD_ADD_YELLOW = Helpers.stringToBase4("PIPIICF");
    public final static int CMD_ADD_BLUE = Helpers.stringToBase4("PIPIICP");
    public final static int CMD_ADD_MAGENTA = Helpers.stringToBase4("PIPIIFC");
    public final static int CMD_ADD_CYAN = Helpers.stringToBase4("PIPIIFF");
    public final static int CMD_ADD_WHITE = Helpers.stringToBase4("PIPIIPC");
    public final static int CMD_ADD_TRANSPARENT = Helpers.stringToBase4("PIPIIPF");
    public final static int CMD_ADD_OPAQUE = Helpers.stringToBase4("PIPIIPP");
    public final static int CMD_CLEAR_BUCKET = Helpers.stringToBase4("PIIPICP");
    public final static int CMD_MOVE = Helpers.stringToBase4("PIIIIIP");
    public final static int CMD_TURN_CCW = Helpers.stringToBase4("PCCCCCP");
    public final static int CMD_TURN_CW = Helpers.stringToBase4("PFFFFFP");
    public final static int CMD_MARK = Helpers.stringToBase4("PCCIFFP");
    public final static int CMD_LINE = Helpers.stringToBase4("PFFICCP");
    public final static int CMD_TRYFILL = Helpers.stringToBase4("PIIPIIP");
    public final static int CMD_ADD_BITMAP = Helpers.stringToBase4("PCCPFFP");
    public final static int CMD_COMPOSE = Helpers.stringToBase4("PFFPCCP");
    public final static int CMD_CLIP = Helpers.stringToBase4("PFFICCF");

    private final static int ALPHA_MASK = 0xff000000;
    private final static int RED_MASK = 0xff0000;
    private final static int GREEN_MASK = 0xff00;
    private final static int BLUE_MASK = 0xff;
    private final static int ALPHA_SHIFT = 24;
    private final static int RED_SHIFT = 16;
    private final static int GREEN_SHIFT = 8;
    private final static int BLUE_SHIFT = 0;

    private final static int COLOR_BLACK = 0x000000;
    private final static int COLOR_RED = 0xff0000;
    private final static int COLOR_GREEN = 0xff00;
    private final static int COLOR_BLUE = 0xff;
    private final static int COLOR_YELLOW = 0xffff00;
    private final static int COLOR_MAGENTA = 0xff00ff;
    private final static int COLOR_CYAN = 0xffff;
    private final static int COLOR_WHITE = 0xffffff;
    private final static int TRANSPARENT = 0;
    private final static int OPAQUE = 0xff;

    private RnaObserver observer;
    private List<Integer> bucket;
    private List<Integer> alphas;
    private int[] pos;
    private int[] mark;
    private Dir dir;
    private List<Bitmap> bitmaps;
}
