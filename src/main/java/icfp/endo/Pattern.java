package icfp.endo;

import java.util.ArrayList;
import java.util.List;
import icfp.endo.Tokenizer.Token;

/**
 * Pattern
 */
public class Pattern {
    public static class Item {
        private char base;
        private char parens;
        private int skip;
        private String search;

        public Item(char ch) {
            this(ch, -1, null);
        }

        public Item(int skip) {
            this('\0', skip, null);
        }

        public Item(String search) {
            this('\0', -1, search);
        }

        private Item(char ch, int skip, String search) {
            if (ch == '(' || ch == ')') {
                this.parens = ch;
                this.base = '\0';
            } else if (ch != '\0') {
                this.parens = '\0';
                this.base = ch;
            }

            this.skip = skip;
            this.search = search;
        }

        @Override
        public String toString() {
            if (hasBase()) {
                return "" + base;
            } else if (hasSkip()) {
                return "!" + skip;
            } else if (hasSearch()) {
                return "<" + search + ">";
            } else if (hasParens()) {
                return "" + parens;
            }

            return "";
        }

        public boolean hasBase() {
            return base != '\0';
        }

        public boolean hasParens() {
            return parens != '\0';
        }

        public boolean hasSkip() {
            return skip >= 0;
        }

        public boolean hasSearch() {
            return search != null;
        }

        public char getBase() {
            return base;
        }

        public char getParens() {
            return parens;
        }

        public String getSearch() {
            return search;
        }

        public int getSkip() {
            return skip;
        }
    }

    public Pattern(DnaObserver observer) {
        items = new ArrayList<>();
        level = 0;
        shouldReturn = false;
        this.observer = observer;
    }

    public void parse(DNAIterator iter) {
        var tokenizer = new Tokenizer();

        while (true) {
            var token = tokenizer.nextToken(iter);
            if (token == Token.NONE) {
                observer.finish();
                throw new RuntimeException("PATTERN NO TOKEN");
            }

            var item = getItem(token, iter);

            if (shouldReturn) {
                return;
            }

            if (item != null) {
                items.add(item);
            }
        }
    }

    public List<Item> getItems() {
        return items;
    }

    public String toString() {
        var str = new StringBuilder();

        for (var item : items) {
            str.append(item.toString());
        }

        return str.toString();
    }

    private Item getItem(Token token, DNAIterator iter) {
        switch (token) {
        case C:
            observer.addCost(1);
            return new Item('I');
        case F:
            observer.addCost(1);
            return new Item('C');
        case P:
            observer.addCost(1);
            return new Item('F');
        case IC:
            observer.addCost(2);
            return new Item('P');
        case IP:
            observer.addCost(2);
            return new Item(Helpers.nat(iter, observer));
        case IF:
            iter.next(); // Skip one, just because
            observer.addCost(3);
            return new Item(Helpers.consts(iter, observer));
        case IIP:
            observer.addCost(3);
            level += 1;
            return new Item('(');
        case IIC:
        case IIF:
            observer.addCost(3);
            if (level == 0) {
                shouldReturn = true;
                return null;
            }

            level -= 1;
            return new Item(')');
        case III:
            observer.rna(Helpers.rnaToBase4(iter));
            observer.addCost(10);
            return null;
        default:
            observer.finish();
            throw new RuntimeException("Pattern FINISH");
        }
    }

    private List<Item> items;
    private int level;
    private boolean shouldReturn;
    private DnaObserver observer;
}
