package icfp.endo;

import java.util.ArrayList;
import java.util.List;
import java.util.Stack;
import icfp.endo.Template.Item;
import icfp.endo.fastdna.FastDNA;

/**
 * MatchReplace
 */
public class MatchReplace {
    public MatchReplace(DnaObserver observer) {
        this.observer = observer;
    }

    public void execute(DNAIterator iter, List<Pattern.Item> p, List<Template.Item> t) {
        var indices = new Stack<DNAIterator>();
        var env = new ArrayList<DNA>();

        for (var item : p) {
            if (item.hasBase()) {
                observer.addCost(1);
                if (item.getBase() == iter.peek()) {
                    iter.next();
                } else {
                    return;
                }
            } else if (item.hasSkip()) {
                iter.seek(item.getSkip());
                if (!iter.isValid()) {
                    return;
                }
            } else if (item.hasParens()) {
                if (item.getParens() == '(') {
                    indices.push(iter.copy());
                } else {
                    var start = indices.pop();
                    var e = start.substring(iter);

                    env.add(e);
                }
            } else if (item.hasSearch()) {
                var srch = item.getSearch();

                var newIter = Helpers.search(iter, srch);
                if (newIter == null) {
                    observer.addCost(iter.getDNA().length() - iter.getPosition());
                    return;
                }

                newIter.seek(srch.length());
                observer.addCost(newIter.getPosition() - iter.getPosition() - 1);
                iter = newIter;
            } else {
                throw new RuntimeException("MatchReplace.execute UNHANDLED PATTERN ITEM " + item);
            }
        }

        iter.truncate();

        if (Helpers.DEBUG) {
            for (var ndx = 0; ndx < env.size(); ndx += 1) {
                System.out.println("env[" + ndx + "]: " + env.get(ndx).debugString());
            }
        }

        replace(iter, env, t);
    }

    private void replace(DNAIterator iter, List<DNA> env, List<Item> t) {
        var r = Helpers.createDNA();
        var bases = new StringBuilder();

        for (var item : t) {
            if (!item.hasBase() && bases.length() > 0) {
                r.append(bases.toString().getBytes());
                bases = new StringBuilder();
            }

            if (item.hasBase()) {
                bases.append(item.getBase());
            } else if (item.hasReference()) {
                var e = env.get((int) item.getNumber());
                var prot = Helpers.protect(item.getLevel(), e, observer);

                r.append(prot);
            } else if (item.hasLengthIndex()) {
                var e = env.get(item.getLenIndex());
                var nat = Helpers.asnat(e.length());

                r.append(nat.getBytes());
            } else {
                throw new RuntimeException("UNHANDLED TEMPLATE ITEM " + item);
            }
        }

        if (bases.length() > 0) {
            r.append(bases.toString().getBytes());
        }

        var dna = iter.getDNA();

        if (Helpers.DEBUG) {
            System.out.println("replace prepend " + dna.debugString() + " " + r.length());
        }

        if (r.length() > 0) {
            if (r instanceof FastDNA) {
                ((FastDNA) r).collapse();
            }
            dna.prepend(r);
        }
    }

    private DnaObserver observer;
}
