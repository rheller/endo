package icfp.endo;

/**
 * DNA
 */
public interface DNA {
    void prepend(String toPrepend);

    void prepend(byte[] toPrepend);

    void prepend(DNA toPrepend);

    void append(String toAppend);

    void append(byte[] toAppend);

    void append(DNA toAppend);

    DNAIterator getIterator();

    DNAIterator copyIterator(DNAIterator iter);

    String debugString();

    int length();
}
