package icfp.endo;

/**
 * DNAProcessor
 */
public class DNAProcessor {
    public DNAProcessor(DNA dna, DnaObserver observer) {
        this.dna = dna;
        this.observer = observer;
    }

    public void process() {
        while (true) {
            if (Helpers.DEBUG) {
                System.out.println();
                System.out.println("Loop " + observer.getLoop());
            }

            execute();

            observer.incLoop();
        }
    }

    private void execute() {
        var iter = dna.getIterator();
        var pattern = new Pattern(observer);
        var template = new Template(observer);
        var matchreplace = new MatchReplace(observer);

        pattern.parse(iter);
        template.parse(iter);

        if (Helpers.DEBUG) {
            System.out.println("Pattern: " + pattern);
            System.out.println("Template: " + template);
        }

        iter.truncate();

        matchreplace.execute(iter, pattern.getItems(), template.getItems());

        if (Helpers.DEBUG) {
            System.out.println("After execute " + dna.debugString());
        }
    }

    private DNA dna;
    private DnaObserver observer;
}
