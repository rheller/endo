package icfp.endo;

/**
 * RnaObserver
 */
public interface RnaObserver {
    void image(int[][] img);
}
