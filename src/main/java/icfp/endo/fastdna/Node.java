package icfp.endo.fastdna;

/**
 * Node
 */
public class Node {
    public Node(SharedBuffer buffer) {
        this.buffer = buffer;
        this.next = null;
    }

    @Override
    public String toString() {
        return buffer.toString();
    }

    public SharedBuffer getBuffer() {
        return buffer;
    }

    public int length() {
        return buffer.length();
    }

    public byte getByte(int offset) {
        return buffer.get(offset);
    }

    public Node getNext() {
        return next;
    }

    public void setNext(Node next) {
        this.next = next;
    }

    private SharedBuffer buffer;
    private Node next;
}
