package icfp.endo.fastdna;

import java.util.ArrayList;
import java.util.List;
import icfp.endo.DNA;
import icfp.endo.DNAIterator;

/**
 * FastDNA
 */
public class FastDNA implements DNA {
    public FastDNA() {
    }

    protected FastDNA(Node n) {
        head = n;
        tail = n;
    }

    public int nodeCount() {
        int count = 0;

        var node = head;
        while (node != tail.getNext()) {
            count += 1;
            node = node.getNext();
        }

        return count;
    }

    @Override
    public void append(String toAppend) {
        throw new RuntimeException("FastDNA.append(String) DO NOT USE THIS, CALL WITH BYTE[]");
    }

    protected void append(SharedBuffer buffer) {
        var node = new Node(buffer);

        if (initializeList(node)) {
            return;
        }

        tail.setNext(node);
        tail = node;
    }

    @Override
    public void append(byte[] toAppend) {
        append(new SharedBuffer(toAppend));
    }

    @Override
    public void append(DNA toAppend) {
        var inDna = (FastDNA) toAppend;

        if (head == null) {
            head = inDna.head;
            tail = inDna.tail;
            return;
        }

        var node = inDna.head;
        while (node != inDna.tail.getNext()) {
            var newNode = new Node(node.getBuffer());

            tail.setNext(newNode);
            tail = newNode;

            node = node.getNext();
        }
    }

    public void collapse() {
        var node = head;

        node = getCollapsed(head);
        if (node != head) {
            head = node;
            if (node.getNext() == null) {
                tail = node;
            }
        }

        var prev = node;
        node = node.getNext();
        while (node != tail.getNext()) {
            var newNode = getCollapsed(node);

            if (newNode != node) {
                prev.setNext(newNode);
                prev = newNode;
                if (newNode.getNext() == null) {
                    tail = newNode;
                }
                node = newNode.getNext();
            } else {
                prev = node;
                node = node.getNext();
            }
        }
    }

    private Node getCollapsed(Node node) {
        var list = new ArrayList<SharedBuffer>();
        var length = 0;

        while (node != null && node.length() < 1000) {
            var nodeBuffer = node.getBuffer();

            length += nodeBuffer.length();
            list.add(node.getBuffer());

            node = node.getNext();
        }

        if (length == 0) {
            return node;
        }

        var combined = combineBuffers(length, list);
        var newNode = new Node(combined);
        newNode.setNext(node);

        return newNode;
    }

    private SharedBuffer combineBuffers(int totalLength, List<SharedBuffer> list) {
        var outBuf = new byte[totalLength];
        var offset = 0;

        for (var buf : list) {
            var bufLength = buf.length();

            System.arraycopy(buf.getByteArray(), 0, outBuf, (int) offset, (int) bufLength);

            offset += bufLength;
        }

        return new SharedBuffer(outBuf);
    }

    @Override
    public DNAIterator copyIterator(DNAIterator iter) {
        throw new RuntimeException("FastDNA.copyIterator NOT DONE");
    }

    @Override
    public String debugString() {
        var str = new StringBuilder();
        var iter = getIterator();
        var count = 0;

        while (iter.hasNext() && count < 10) {
            str.append(iter.next());
            count += 1;
        }

        if (length() > 10) {
            str.append("...");
        }

        str.append(" (").append(length()).append(")");

        return str.toString();
    }

    @Override
    public DNAIterator getIterator() {
        return new Iterator(this);
    }

    @Override
    public int length() {
        var node = head;
        var length = 0;

        if (node == null) {
            return 0;
        }

        var nodeCount = 0;
        while (node != null && node != tail.getNext()) {
            nodeCount += 1;
            length += node.length();
            node = node.getNext();
        }

        if (nodeCount > 200) {
            combineAllNodes();
        }

        return length;
    }

    private void combineAllNodes() {
        var toCombine = new ArrayList<SharedBuffer>();
        var node = head;
        var length = 0;

        while (node != tail.getNext()) {
            length += node.length();
            toCombine.add(node.getBuffer());
            node = node.getNext();
        }

        var combined = combineBuffers(length, toCombine);
        var newNode = new Node(combined);

        head = newNode;
        tail = newNode;
    }

    @Override
    public void prepend(String toPrepend) {
        throw new RuntimeException("FastDNA.prepend(String) DO NOT USE THIS, CALL WITH BYTE[]");
    }

    @Override
    public void prepend(byte[] toPrepend) {
        var newNode = createNode(toPrepend);

        if (initializeList(newNode)) {
            return;
        }

        newNode.setNext(head);
        head = newNode;
    }

    @Override
    public void prepend(DNA toPrepend) {
        var inDna = (FastDNA) toPrepend;

        if (head == null) {
            head = inDna.head;
            tail = inDna.tail;
            return;
        }

        if (inDna.tail == null) {
            System.out.println("TAIL IS NULL " + toPrepend.debugString());
        }

        inDna.tail.setNext(head);
        head = inDna.head;
    }

    @Override
    public String toString() {
        var str = new StringBuilder();
        var node = head;

        while (node != null) {
            str.append(node.toString());
            node = node.getNext();
        }

        return str.toString();
    }

    public void debugStructure() {
        System.out.println("DNA");

        var node = head;
        do {
            if (node.length() < 200) {
                System.out.println("-- " + node + " (" + node.length() + ")");
            } else {
                System.out.println("-- (" + node.length() + ")");
            }
            node = node.getNext();
        } while (node != null && node != tail.getNext());
    }

    public Node getHead() {
        return head;
    }

    public void setHead(Node head) {
        this.head = head;
    }

    public void setTail(Node tail) {
        this.tail = tail;
    }

    public Node getTail() {
        return tail;
    }

    private Node createNode(byte[] data) {
        var buffer = new SharedBuffer(data);
        return new Node(buffer);
    }

    private boolean initializeList(Node newNode) {
        if (head == null) {
            head = newNode;
            tail = newNode;
            return true;
        }

        return false;
    }

    private Node head;
    private Node tail;
}
