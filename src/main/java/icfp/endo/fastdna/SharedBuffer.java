package icfp.endo.fastdna;

import java.util.Arrays;

/**
 * SharedBuffer
 */
public class SharedBuffer {
    public SharedBuffer(byte[] data) {
        if (data.length < 1024) {
            buffer = new byte[1024];
            System.arraycopy(data, 0, buffer, 0, data.length);
        } else {
            buffer = data;
        }

        first = 0;
        last = data.length;
    }

    public SharedBuffer(SharedBuffer copy, int start) {
        buffer = copy.buffer;
        first = copy.first + start;
        last = copy.last;
    }

    public SharedBuffer(SharedBuffer copy, int start, int end) {
        buffer = copy.buffer;
        first = copy.first + start;
        last = copy.first + end - 1;
    }

    public boolean canFit(int length) {
        var remaining = buffer.length - last;
        return remaining > length;
    }

    public void append(byte[] data) {
        System.out.println("append " + last + " " + data.length);
        System.arraycopy(data, 0, buffer, (int) last, data.length);
        last += data.length;
    }

    @Override
    public String toString() {
        var copy = Arrays.copyOfRange(buffer, (int) first, (int) last);
        return new String(copy);
    }

    public byte[] getByteArray() {
        return Arrays.copyOfRange(buffer, (int) first, (int) last);
    }

    public byte get(int offset) {
        if (offset < 0 || (offset + first > buffer.length)) {
            return '\0';
        }
        var ndx = (int) (first + offset);
        return buffer[ndx];
    }

    public int length() {
        return last - first;
    }

    private byte[] buffer;
    private int first;
    private int last;
}
