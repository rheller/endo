package icfp.endo.fastdna;

import icfp.endo.DNA;
import icfp.endo.DNAIterator;

/**
 * Iterator
 */
public class Iterator implements DNAIterator {
    public Iterator(FastDNA dna) {
        this.dna = dna;
        this.curNode = dna.getHead();
        this.prevNode = null;
        this.pos = 0;
        this.rawPos = 0;
    }

    @Override
    public int getPosition() {
        return rawPos;
    }

    @Override
    public DNAIterator copy() {
        var clone = new Iterator(dna);

        clone.pos = pos;
        clone.rawPos = rawPos;
        clone.curNode = curNode;
        clone.prevNode = prevNode;

        return clone;
    }

    @Override
    public DNA getDNA() {
        return this.dna;
    }

    @Override
    public boolean isValid() {
        return curNode != null || prevNode == dna.getTail();
    }

    @Override
    public char peek() {
        if (curNode == null) {
            return '\0';
        }

        byte b = curNode.getByte(pos);

        return (char) b;
    }

    @Override
    public char peek(int offset) {
        var cp = copy();

        cp.seek(offset);

        return cp.peek();
    }

    @Override
    public void seek(int jump) {
        if (curNode == null) {
            return;
        }

        if (jump + pos < curNode.length()) {
            pos += jump;
            rawPos += jump;
            return;
        }

        jump -= curNode.length() - pos;
        rawPos += curNode.length() - pos;
        pos = 0;
        prevNode = curNode;
        curNode = curNode.getNext();

        while (curNode != null && jump >= curNode.length()) {
            rawPos += curNode.length();
            jump -= curNode.length();
            prevNode = curNode;
            curNode = curNode.getNext();
        }

        pos = jump;
        rawPos += jump;
    }

    @Override
    public DNA substring(DNAIterator end) {
        var endIter = (Iterator) end;
        if (curNode == endIter.curNode) {
            var buf = new SharedBuffer(curNode.getBuffer(), pos, endIter.pos + 1);
            var node = new Node(buf);

            return new FastDNA(node);
        }

        var sub = new FastDNA();
        sub.append(new SharedBuffer(curNode.getBuffer(), pos));

        var node = curNode.getNext();
        while (node != endIter.curNode) {
            sub.append(node.getBuffer());
            node = node.getNext();
        }

        if (endIter.curNode != null && endIter.pos > 0) {
            sub.append(new SharedBuffer(endIter.curNode.getBuffer(), 0, endIter.pos + 1));
        }

        return sub;
    }

    @Override
    public void truncate() {
        if (pos == 0) {
            dna.setHead(curNode);
            if (curNode == null) {
                dna.setTail(null);
            }
            return;
        }

        if (curNode == null) {
            dna.setHead(null);
            dna.setTail(null);
            pos = 0;
            return;
        }

        var buf = new SharedBuffer(curNode.getBuffer(), pos);
        var newNode = new Node(buf);

        newNode.setNext(curNode.getNext());
        dna.setHead(newNode);

        curNode = newNode;
        pos = 0;
        rawPos = 0;
    }

    @Override
    public boolean hasNext() {
        if (curNode == null) {
            return false;
        }

        return pos < curNode.length();
    }

    @Override
    public Character next() {
        char ch = peek();

        if (ch == '\0') {
            return ch;
        }

        pos += 1;
        rawPos += 1;

        if (pos >= curNode.length()) {
            pos = 0;
            prevNode = curNode;
            curNode = curNode.getNext();
        }

        return ch;
    }

    private FastDNA dna;
    private Node curNode;
    private Node prevNode;
    private int pos;
    private int rawPos;
}
