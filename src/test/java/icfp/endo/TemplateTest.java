package icfp.endo;

import static org.junit.Assert.assertEquals;
import org.junit.Test;

/**
 * TemplateTest
 */
public class TemplateTest {
    @Test
    public void testBases() {
        runTest("CFPIC", "ICFP");
    }

    @Test
    public void testReference() {
        runTest("IFPPIPPP", "[0_0][0_0]");
        runTest("IFICPCPIPIICPCCP", "[1_2][3_4]");
    }

    @Test
    public void testIndex() {
        runTest("IIPICP", "|2|");
    }

    private void runTest(String inDNA, String expTemplate) {
        var dna = Helpers.createDNA();
        var t = new Template(new TestObserver());

        dna.prepend(inDNA);

        t.parse(dna.getIterator());
        assertEquals(expTemplate, t.toString());
    }
}
