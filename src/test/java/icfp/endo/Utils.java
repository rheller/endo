package icfp.endo;

import java.util.Random;

/**
 * Utils
 */
public class Utils {
    public static DNA makeAlphabetDNA() {
        var dna = Helpers.createDNA();

        dna.append("abc");
        dna.append("def");
        dna.append("ghi");
        dna.append("jkl");
        dna.append("mno");
        dna.append("pqr");
        dna.append("stu");
        dna.append("vwx");
        dna.append("yz");

        return dna;
    }

    public static DNA makeRandomDNA(int chunkSize, int count) {
        var dna = Helpers.createDNA();

        for (var loop = 0; loop < count; loop += 1) {
            var str = randomString(chunkSize);

            dna.append(str);
        }

        return dna;
    }

    private static byte[] randomString(int size) {
        var buf = new byte[size];
        var letters = new byte[] {'A', 'B', 'C', 'D', 'E', 'F', 'G', 'H', 'I', 'J', 'K', 'L', 'M',
                'N', 'O', 'P', 'Q', 'R', 'S', 'T', 'U', 'V', 'W', 'X', 'Y', 'Z'};

        for (var ndx = 0; ndx < size; ndx += 1) {
            buf[ndx] = letters[rand.nextInt(letters.length)];
        }

        return buf;
    }

    private static final Random rand = new Random();
}
