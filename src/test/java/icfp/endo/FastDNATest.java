package icfp.endo;

import static org.junit.Assert.assertEquals;
import org.junit.Test;
import icfp.endo.fastdna.FastDNA;

/**
 * FastDNATest
 */
public class FastDNATest {
    @Test
    public void testBasics() {
        var dna = Helpers.createDNA();

        dna.prepend("ABCD");

        var iter = dna.getIterator();
        assertEquals('A', iter.peek());
        assertEquals('B', iter.peek(1));
        assertEquals('C', iter.peek(2));
        assertEquals('D', iter.peek(3));
        assertEquals('\0', iter.peek(4));
    }

    @Test
    public void testAppend() {
        var dna = Helpers.createDNA();

        dna.append("A");
        dna.append("B");
        dna.append("C");
        dna.append("D");

        var iter = dna.getIterator();
        assertEquals('A', iter.peek());
        assertEquals('B', iter.peek(1));
        assertEquals('C', iter.peek(2));
        assertEquals('D', iter.peek(3));
        assertEquals('\0', iter.peek(4));
    }

    @Test
    public void testAppendPeek() {
        var dna = Helpers.createDNA();

        dna.append("ABC");
        dna.append("DEF");
        dna.append("GHI");
        dna.append("JKL");

        var iter = dna.getIterator();
        assertEquals('B', iter.peek(1));
        assertEquals('E', iter.peek(4));
        assertEquals('I', iter.peek(8));
        assertEquals('J', iter.peek(9));
        assertEquals('L', iter.peek(11));
    }

    @Test
    public void testAppendNext() {
        var dna = Helpers.createDNA();

        dna.append("ABC");
        dna.append("DEF");
        dna.append("GHI");
        dna.append("JKL");

        var iter = dna.getIterator();
        assertEquals('A', (char) iter.next());
        assertEquals('B', (char) iter.next());
        assertEquals('C', (char) iter.next());
        assertEquals('D', (char) iter.next());
    }

    @Test
    public void testPrepend() {
        var dna = Helpers.createDNA();

        dna.prepend("D");
        dna.prepend("C");
        dna.prepend("B");
        dna.prepend("A");

        var iter = dna.getIterator();
        assertEquals('A', iter.peek());
        assertEquals('B', iter.peek(1));
        assertEquals('C', iter.peek(2));
        assertEquals('D', iter.peek(3));
        assertEquals('\0', iter.peek(4));
    }

    @Test
    public void testPrependPeek() {
        var dna = Helpers.createDNA();

        dna.prepend("JKL");
        dna.prepend("GHI");
        dna.prepend("DEF");
        dna.prepend("ABC");

        var iter = dna.getIterator();
        assertEquals('B', iter.peek(1));
        assertEquals('E', iter.peek(4));
        assertEquals('I', iter.peek(8));
        assertEquals('J', iter.peek(9));
        assertEquals('L', iter.peek(11));
    }

    @Test
    public void testSubstring() {
        var dna = Helpers.createDNA();

        dna.append("ABCDEF".getBytes());

        var start = dna.getIterator();
        var end = dna.getIterator();

        start.seek(1);
        end.seek(3);

        var sub = start.substring(end);
        assertEquals("BC", sub.toString());
    }

    @Test
    public void testSplitSubstring() {
        var dna = Helpers.createDNA();

        dna.append("ABC".getBytes());
        dna.append("DEF".getBytes());
        dna.append("GHI".getBytes());

        runSubstringTest(dna, 0, 1, "A");
        runSubstringTest(dna, 1, 2, "B");
        runSubstringTest(dna, 1, 3, "BC");
        runSubstringTest(dna, 1, 5, "BCDE");
        runSubstringTest(dna, 0, 6, "ABCDEF");
        runSubstringTest(dna, 0, 8, "ABCDEFGH");
        runSubstringTest(dna, 0, 9, "ABCDEFGHI");
    }

    @Test
    public void testProfile() {
        var dna = Helpers.createDNA();

        for (var loop = 0; loop < 10000000; loop += 1) {
            dna.append("ABC");
        }
        // System.out.println("length " + dna.length());
    }

    @Test
    public void testCollapse() {
        var bigChunk = new byte[2000];
        var dna = new FastDNA();

        for (var ndx = 0; ndx < bigChunk.length; ndx += 1) {
            bigChunk[ndx] = 'b';
        }

        for (var loop = 0; loop < 5; loop += 1) {
            dna.append(new byte[] {'a'});
        }

        dna.append(bigChunk);

        for (var loop = 0; loop < 6; loop += 1) {
            dna.append(new byte[] {'c'});
        }

        dna.debugStructure();
        dna.collapse();
        dna.debugStructure();
        System.out.println("tail " + dna.getTail().length());
    }

    private void runSubstringTest(DNA dna, int start, int end, String expected) {
        var startIter = dna.getIterator();
        var endIter = dna.getIterator();

        startIter.seek(start);
        endIter.seek(end);

        var sub = startIter.substring(endIter);
        assertEquals(expected, sub.toString());
    }
}
