package icfp.endo;

import static org.junit.Assert.assertEquals;
import static org.junit.Assert.assertFalse;
import static org.junit.Assert.assertTrue;
import org.junit.Test;
import icfp.endo.fastdna.SharedBuffer;

/**
 * SharedBufferTest
 */
public class SharedBufferTest {
    @Test
    public void testAppend() {
        var data = new byte[] {1, 2};
        var buffer = new SharedBuffer(data);

        buffer.append(new byte[] {3, 4});
        buffer.append(new byte[] {5});

        assertEquals(1, buffer.get(0));
        assertEquals(2, buffer.get(1));
        assertEquals(3, buffer.get(2));
        assertEquals(4, buffer.get(3));
        assertEquals(5, buffer.get(4));

        assertFalse(buffer.canFit(1024));
        assertTrue(buffer.canFit(10));
        assertTrue(buffer.canFit(1018));
    }
}
