package icfp.endo;

import static org.junit.Assert.assertEquals;
import static org.junit.Assert.assertNull;
import org.junit.Test;

/**
 * SearchTest
 */
public class SearchTest {
    @Test
    public void testBasics() {
        var dna = Utils.makeAlphabetDNA();
        var iter = dna.getIterator();

        var srchIter = Helpers.search(iter, "hijklmnop");

        assertEquals('h', srchIter.peek());
    }

    @Test
    public void testFailure() {
        var dna = Helpers.createDNA();

        dna.append("aabbcddcbbaa");

        var iter = dna.getIterator();
        var srchIter = Helpers.search(iter, "aaabbb");

        assertNull(srchIter);
    }

    @Test
    public void testLargString() {
        var dna = Utils.makeRandomDNA(2, 10000000);

        dna.append(new byte[] {'c'});

        var iter = dna.getIterator();
        var srchIter = Helpers.search(iter, "c");

        assertEquals(srchIter.peek(), 'c');
    }
}
