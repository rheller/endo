package icfp.endo;

/**
 * TestObserver
 */
public class TestObserver implements DnaObserver {

    @Override
    public void addCost(int value) {

    }

    @Override
    public void finish() {

    }

    @Override
    public int getCost() {
        return 0;
    }

    @Override
    public int getLoop() {
        return 0;
    }

    @Override
    public int getRnaCount() {
        return 0;
    }

    @Override
    public void incLoop() {

    }

    @Override
    public void rna(int item) {

    }

}
