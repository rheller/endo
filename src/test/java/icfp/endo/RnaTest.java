package icfp.endo;

import java.util.ArrayList;
import java.util.Arrays;
import org.junit.Test;

/**
 * RnaTest
 */
public class RnaTest {
    @Test
    public void testBucket() {
        var rna = new RNAProcessor(new TestRnaObserver());
        var cmds = new ArrayList<Integer>();

        for (var i = 0; i < 18; i += 1) {
            cmds.add(RNAProcessor.CMD_ADD_BLACK);
        }
        for (var i = 0; i < 7; i += 1) {
            cmds.add(RNAProcessor.CMD_ADD_RED);
        }
        for (var i = 0; i < 39; i += 1) {
            cmds.add(RNAProcessor.CMD_ADD_MAGENTA);
        }
        for (var i = 0; i < 10; i += 1) {
            cmds.add(RNAProcessor.CMD_ADD_WHITE);
        }
        for (var i = 0; i < 3; i += 1) {
            cmds.add(RNAProcessor.CMD_ADD_OPAQUE);
        }
        cmds.add(RNAProcessor.CMD_ADD_TRANSPARENT);

        rna.process(cmds);

        System.out.println("pixel " + String.format("0x%x", rna.currentPixel()));
    }
}
