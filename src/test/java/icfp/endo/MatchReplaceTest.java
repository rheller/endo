package icfp.endo;

import static org.junit.Assert.assertEquals;
import java.util.Arrays;
import java.util.List;
import org.junit.Test;

/**
 * MatchReplaceTest
 */
public class MatchReplaceTest {
    @Test
    public void testBasics() {
        runTest(
                "IICCPPII",
                Arrays.asList(
                        new Pattern.Item(0),
                        new Pattern.Item('('),
                        new Pattern.Item(2),
                        new Pattern.Item('('),
                        new Pattern.Item(2),
                        new Pattern.Item(')'),
                        new Pattern.Item(2),
                        new Pattern.Item(')')),
                Arrays.asList(
                        new Template.Item(0, 0),
                        new Template.Item(0, 1),
                        new Template.Item('I'),
                        new Template.Item('I')),
                "CCIICCPPIIII");
    }

    private void runTest(String inDNA, List<Pattern.Item> p, List<Template.Item> t, String expDNA) {
        var dna = Helpers.createDNA();
        var mr = new MatchReplace(new TestObserver());

        dna.prepend(inDNA.getBytes());

        var iter = dna.getIterator();

        mr.execute(iter, p, t);

        assertEquals(expDNA, dna.toString());
    }
}
