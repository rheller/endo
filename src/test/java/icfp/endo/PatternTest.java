package icfp.endo;

import static org.junit.Assert.assertEquals;
import org.junit.Test;

/**
 * PatternTest
 */
public class PatternTest {
    @Test
    public void testBases() {
        runTest("CFPIC", "ICFP");
    }

    @Test
    public void testSkip() {
        runTest("IPICCIICP", "!38");
    }

    @Test
    public void testSearch() {
        runTest("IFCCFPICIIF", "<ICFP>");
        runTest("IFCCFPICCFPICIIF", "<ICFPICFP>");
    }

    @Test
    public void testParens() {
        runTest("IIPCFPICIIF", "(ICFP)");
    }

    @Test
    public void testRNA() {
        runTest("IIICCCCCCCIIPCFPICIIF", "(ICFP)");
    }

    private void runTest(String inDNA, String expPattern) {
        var dna = Helpers.createDNA();
        var p = new Pattern(new TestObserver());

        dna.prepend(inDNA);

        p.parse(dna.getIterator());
        assertEquals(expPattern, p.toString());
    }
}
