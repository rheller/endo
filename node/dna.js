const cp = require('child_process');

const classpath = ['../target/classes',
    '/home/rich/.m2/repository/com/google/code/gson/gson/2.8.5/gson-2.8.5.jar'].join(':');

module.exports.start = (dnaFileName, prefixFileName, observer) => {
    const baseArgs = ['-cp', classpath, 'icfp.endo.App'];
    const statsArgs = ['-stats', 10000];
    const dnaArgs = ['-dna', dnaFileName];
    let prefixArgs = [];

    if (prefixFileName !== '') {
        prefixArgs = ['-prefix', prefixFileName];
    }

    const java = cp.spawn('java', baseArgs.concat(statsArgs).concat(dnaArgs).concat(prefixArgs));
    const begin = Date.now();

    java.stdout.on('data', (data) => {
        try {
            observer(data.toString());
        } catch (err) { }
    });

    java.stderr.on('data', (data) => {
        console.log(`ERROR: ${data}`);
    });

    java.on('close', () => {
        console.log('FINISHED', (Date.now() - begin) / 1000.0);
    });
};
