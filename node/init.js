const express = require('express');
const bodyParser = require('body-parser');
const ws = require('ws');
const fs = require('fs');
const path = require('path');

const dna = require('./dna');

const app = express();
const router = express.Router();
const port = 8080;

let clientSocket = null;

router.get('/status', (req, res) => {
    res.json({ status: 'All good' });
});

router.get('/dna_files', async (req, res) => {
    const files = await getFileList('../dna');
    sendResult(res, 200, JSON.stringify(files));
});

router.get('/prefix_files', async (req, res) => {
    const files = await getFileList('../prefix');
    sendResult(res, 200, JSON.stringify(files));
});

router.post('/start', (req, res) => {
    const dnaFileName = req.body.dna;
    const prefixFileName = req.body.prefix;

    if (clientSocket === null) {
        return sendResult(res, 403, 'No data connection, bailing out');
    }

    if (dnaFileName !== '') {
        var dnaFile = path.join('../dna', dnaFileName);
        var prefixFile = '';

        if (prefixFileName !== '') {
            prefixFile = path.join('../prefix', prefixFileName);
        }

        dna.start(dnaFile, prefixFile, (data) => {
            clientSocket.send('' + data);
        });
    }

    sendResult(res, 200, '');
});

app.use(express.static('static'));
app.use(bodyParser.json());
app.use('/', router);

const server = app.listen(port, () => {
    console.log(`Listening on ${port}`);
});

const wsServer = new ws.Server({ server });
wsServer.on('connection', (ws) => {
    ws.on('message', (msg) => {
        console.log(`Message: ${msg}`);
    });

    clientSocket = ws;
});

function getFileList(path) {
    return new Promise((resolve, reject) => {
        fs.readdir(path, (err, items) => {
            if (err) {
                return reject(err);
            }

            const files = [];

            for (let item of items) {
                files.push(item);
            }

            resolve(files);
        });

    });
}

function sendResult(res, status, data) {
    res.status(status);
    res.send(data);
}
