function requestDnaFiles(ws) {
    const json = {
        request: "dna_files"
    };

    console.log('sending', json);
    ws.send(JSON.stringify(json));
}
