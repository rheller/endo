function wsConnect() {
    return new Promise((resolve, reject) => {
        const host = document.location.host;
        const path = document.location.pathname;

        const ws = new WebSocket(`ws://${host}${path}endo`);

        ws.onopen = (event) => {
            resolve(ws);
        };
    });
}
