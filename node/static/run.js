function handleMessage(event) {
    const json = parseData(event.data);

    if (json !== null) {
        document.getElementById('loop').innerHTML = json.loops;
        document.getElementById('cost').innerHTML = json.cost;
        document.getElementById('rna').innerHTML = json.rna;
        if (json.image !== undefined) {
            document.getElementById('image').src = json.image;
        }
    }
}

function parseData(data) {
    try {
        const json = JSON.parse(data);
        return json;
    } catch (err) {
        return null;
    }
}
async function startEndo() {
    const dnaSelect = document.getElementById('dna-select');
    const dnaFile = dnaSelect.options[dnaSelect.selectedIndex].value;

    const prefixSelect = document.getElementById('prefix-select');
    const prefixFile = prefixSelect.options[prefixSelect.selectedIndex].value;

    const payload = {
        dna: dnaFile,
        prefix: prefixFile
    };

    try {
        const ws = await wsConnect();
        ws.onmessage = handleMessage;

        await axios.post('/start', payload);
    } catch (err) {
        console.log(err);
        document.getElementById('msg').innerHTML = err;
    }
}

function setFileSelectOptions(selectId, files) {
    let html = '';

    html += `<option value="">-- NONE --</option>`;
    for (const item of files) {
        html += `<option value="${item}">${item}</option>`;
    }

    document.getElementById(selectId).innerHTML = html;
}

(async () => {
    const dnaFilesResult = await axios.get('/dna_files');
    const prefixFilesResult = await axios.get('/prefix_files');

    if (dnaFilesResult.data !== undefined) {
        setFileSelectOptions('dna-select', dnaFilesResult.data);
    }

    if (prefixFilesResult.data !== undefined) {
        setFileSelectOptions('prefix-select', prefixFilesResult.data);
    }
})();
